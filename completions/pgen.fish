function pgen_completions

  # Load dependency
  source (command dirname (status -f))/../dependency.fish
  and dependency -P https://gitlab.com/lusiadas/contains_opts
  or return 1

  # Add completions
  set -l cmd (basename (status -f) | cut -d '.' -f 1)
  set -l opts r retrieve c clipboard a add t trim o overwrite w without-tor p password
  complete -c $cmd -n "not contains_opts (string match -rv -- '^(c|clipboard|p|password)\$' $opts)" \
  -s r -l retrieve -d 'Retrieve passphrases from a wordlist'
  complete -c $cmd -n "not contains_opts (string match -rv -- '^(r|retrieve|p|password)\$' $opts)" \
  -s c -l clipboard -d 'Have the resulting passphrase sent directly to the clipboard'
  complete -c $cmd -n "not contains_opts (string match -rv -- '^(w|without-tor|p|password)\$' $opts)" \
  -s a -l add -d 'Add words to a wordlist'
  complete -c $cmd -n "not contains_opts (string match -rv -- '^(w|without-tor|p|password)\$' $opts)" \
  -s t -l trim -d 'Trim words from a wordlist'
  complete -c $cmd -n "not contains_opts (string match -rv -- '^(w|without-tor|p|password)\$' $opts)" \
  -s o -l overwrite -d 'Overwrite a wordlist'
  complete -c $cmd -n "not contains_opts (string match -r -- '^(r|retrieve|c|clipboard|p|password)\$' $opts)" \
  -s w -l without-tor -d 'Disable drawing words through the tor network'
  complete -xc $cmd -s p -l password -d 'Set decryption password'
  complete -c $cmd -n 'not contains_opts' -s n -l new-password -d 'Change the decryption password of a file'
  complete -c $cmd -n 'not contains_opts' -s h -l help -d \
  'Display instructions'
end

pgen_completions
functions -e aux_plugin pgen_completions

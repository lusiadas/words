# pgen

> A plugin for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish).

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v3.0.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-blue.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)


<br/>

## Description

**Passphrase Generator**. Generate wordlists using [Wordnik's dictionary](https://wordnik.com), and then, retrieve passphrases from it.

## Synopsis

```
**pgen** [--retrive/--add] [WORDLIST] [PGP KEY] [QUANTITY]
```

## Options


- `-r/--retrieve`

Retrieve passphrases from a wordlist. This is the default option. If no quantity of word is described, `5` words are drawn. If no wordlist is described, `$path/default.gpg` is used. If no PGP keypair is described, the first private key available is used. If the wordlist hasn't enough words to build the requested passphrase, words shall be added to the wordlist (see option `-a/--add`).

To have the resulting passphrase sent directly to your clipboard, add the modifier flag `-c/--clipboard"`.

- `-a/--add"$reg`

Add words to a wordlist. If no quantity is described, words will keep on being added until the process is interrupted with <kbd>Ctrl</kbd> + <kbd>Z</kbd>. As for the wordlist and private key parameters, the same rules from the option `-r/--retrieve` apply.

Words are draw from the Wordnik's dictionary through the tor network. This is a slower method, but one that protects the confidentiality of the words drawn from traffic analysis. To draw words faster, you can disable that protection by accessing Wordnik's random word page directly with `-w/--without-tor"$reg`.

- `-h/--help`

Display these instructions

## Install

```fish
omf install words
```

___

Ⓐ Made in Anarchy. No wage slaves were economically coerced into the making of this work.
